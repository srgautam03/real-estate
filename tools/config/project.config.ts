import { join } from 'path';

import { SeedConfig } from './seed.config';
import { ExtendPackages } from './seed.config.interfaces';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

  constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';
    // this.GOOGLE_ANALYTICS_ID = 'Your site's ID';

    /* Enable typeless compiler runs (faster) between typed compiler runs. */
    // this.TYPED_COMPILE_INTERVAL = 5;

    // Add `NPM` third-party libraries to be injected/bundled.
    this.NPM_DEPENDENCIES = [
      ...this.NPM_DEPENDENCIES,
      // {src: 'jquery/dist/jquery.min.js', inject: 'libs'},
      // {src: 'lodash/lodash.min.js', inject: 'libs'},
      { src: 'js-marker-clusterer/src/markerclusterer.js', inject: 'libs' }
    ];

    // Add `local` third-party libraries to be injected/bundled.
    this.APP_ASSETS = [
      // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
      // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
    ];

    this.ROLLUP_INCLUDE_DIR = [
      ...this.ROLLUP_INCLUDE_DIR,
      //'node_modules/moment/**'
    ];

    this.ROLLUP_NAMED_EXPORTS = [
      ...this.ROLLUP_NAMED_EXPORTS,
      //{'node_modules/immutable/dist/immutable.js': [ 'Map' ]},
    ];

    // Add packages (e.g. ng2-translate)
    const additionalPackages: ExtendPackages[] = [{
      // name: 'ng2-translate',
      // // Path to the package's bundle
      // path: 'node_modules/ng2-translate/bundles/ng2-translate.umd.js'

      name: 'ngx-pagination',
      path: 'node_modules/ngx-pagination/dist/ngx-pagination.umd.js',
    },
    {
      name: '@agm/core',
      path: 'node_modules/@agm/core/core.umd.js',
    },
    {
      name: '@agm/js-marker-clusterer',
      path: 'node_modules/@agm/js-marker-clusterer/js-marker-clusterer.umd.js',
    },
    {
      name: 'ngx-order-pipe',
      path: 'node_modules/ngx-order-pipe/bundles/ngx-order-pipe.umd.js',
    }
      ,
    {
      name: 'ngx-input-tag',
      path: 'node_modules/ngx-input-tag/bundles/ngx-input-tag.umd.js',
    },
    {
      name: 'ngx-bootstrap',
      path: 'node_modules/ngx-bootstrap',
      packageMeta: {
        main: 'bundles/ngx-bootstrap.umd.min.js',
        defaultExtension: 'js'
      }
    },

    // required for prod build
    {
      name: 'ngx-bootstrap/*',
      path: 'node_modules/ngx-bootstrap/*',
      packageMeta: {
        main: 'bundles/ngx-bootstrap.umd.min.js',
        defaultExtension: 'js'
      }
    }
      // {
      //   name: 'ngx-chips',
      //   path: 'node_modules/ngx-chips/bundles/ngx-chips.umd.js',
      // }
    ];

    this.addPackagesBundles(additionalPackages);

    /* Add proxy middleware */
    // this.PROXY_MIDDLEWARE = [
    //   require('http-proxy-middleware')('/api', { ws: false, target: 'http://localhost:3003' })
    // ];

    /* Add to or override NPM module configurations: */
    // this.PLUGIN_CONFIGS['browser-sync'] = { ghostMode: false };
  }

}
