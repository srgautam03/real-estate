import { NgModule } from '@angular/core';
import { RatedHomesComponent } from './ratedhomes.component';
import { RatedHomesRoutingModule } from './ratedhomes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [RatedHomesRoutingModule, SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBhRJ9XEkenaPc_INsVMdm3MnHQobYHL-0',
            libraries: ['places']
        })],
    declarations: [RatedHomesComponent],
    exports: [RatedHomesComponent]
})
export class RatedHomesModule { }
