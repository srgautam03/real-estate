import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RatedHomesComponent } from './ratedhomes.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'rate-home', component: RatedHomesComponent }
        ])
    ],
    exports: [RouterModule]
})
export class RatedHomesRoutingModule { }
