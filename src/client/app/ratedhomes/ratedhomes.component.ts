import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';

/**
 * This class represents the lazy loaded RatedHomesComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'sd-ratedhomes',
    templateUrl: 'ratedhomes.component.html',
    styleUrls: ['ratedhomes.component.css'],
})
export class RatedHomesComponent implements OnInit {

    address: string;
    showhousingTypePage: boolean;
    showhousingFormPage1: boolean;
    @ViewChild('search')
    public searchElementRef: ElementRef;

    constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    }

    ngOnInit() {
        this.mapsAPILoader.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['address'],
                componentRestrictions: { country: 'de' }
            });
            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    //get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    const description: google.maps.places.PlaceResult.address_components = place.address_components;
                    if (description === null || description === undefined) {
                        return;
                    }

                    this.address = place.formatted_address;
                    // console.log('address ' + this.address);
                    if (this.address !== null || this.address !== '' || this.address !== undefined) {
                        this.showhousingTypePage = true;
                    } else {
                        return;
                    }

                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                });
            });
        });
    }

    showHousingForm1() {
        console.log('inside showHousing Form');
        this.showhousingFormPage1 = true;
    }
}
