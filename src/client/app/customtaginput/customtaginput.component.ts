import { Component, OnInit, forwardRef, Output, Input, EventEmitter, ElementRef, ViewChild, NgZone } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { TypeaheadMatch } from 'ngx-bootstrap';

const noop = () => { };
const $: any = (<any>window)['$'];


const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CustomTagInputComponent),
    multi: true
};

@Component({
    moduleId: module.id,
    selector: 'custom-taginput',
    templateUrl: 'customtaginput.component.html',
    styleUrls: ['customtaginput.component.css'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CustomTagInputComponent implements OnInit, ControlValueAccessor {

    public tags: any[] = [];

    cityName: string;
    @ViewChild('search')
    public searchElementRef: ElementRef;

    @Input() maxTags: number;
    @Input() removeLastOnBackspace: boolean;
    @Input() canDeleteTags = true;
    @Input() placeholder: string;
    @Input() options: any = null;
    @Input() displayField = 'displayValue';
    @Input() minLengthBeforeOptions = 1;
    @Input() scrollableOptions: boolean;
    @Input() scrollableOptionsInView = 5;
    @Output() emitTagsChanged = new EventEmitter();
    @Output() emitMaxTagsReached = new EventEmitter();
    @Output() emitNoOptionFound = new EventEmitter();

    private selected = '';
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;


    constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {

    }

    ngOnInit() {
        this.mapsAPILoader.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['(cities)'],
                componentRestrictions: { country: 'de' }
            });

            autocomplete.addListener('place_changed', () => {
                // console.log("place changed");
                this.ngZone.run(() => {
                    //get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    const description: google.maps.places.PlaceResult.address_components = place.address_components;
                    if (description === null || description === undefined) {
                        return;
                    }
                    //const total = description.length;
                    for (const des of description) {
                        //console.log(des.types[0]);
                        if (des.types[0] === 'locality') {
                            // console.log(des.long_name);
                            this.cityName = des.long_name;
                            // console.log('city name -> ' + this.cityName);
                            const tag = {
                                [this.displayField]: this.cityName
                            };
                            this.addPredefinedTag(tag);
                            this.searchElementRef.nativeElement.value = '';
                            //this.addTag(this.cityName);
                        }
                    }

                    //this.getLatLongForCity(String(this.cityName));

                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                });
            });
        });

    }



    writeValue(value: any) {
        if (value !== this.tags) {
            this.tags = value;
        }
    }

    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    private getPlaceHolder(): string {
        if (this.tags && this.tags.length > 0) {
            return '';
        }
        return this.placeholder;
    }

    private tagsChanged(type: string, tag: any): void {
        this.onChangeCallback(this.tags);
        this.emitTagsChanged.emit({
            change: type,
            tag: tag
        });
        if (this.maximumOfTagsReached()) {
            this.emitMaxTagsReached.emit();
        }
    }

    private removeLastTag(tagInput: HTMLInputElement): void {
        if (!this.removeLastOnBackspace || !this.tags.length) {
            return;
        }

        if (tagInput.value === '') {
            this.removeTag(this.tags[this.tags.length - 1]);
        }
    }



    private addTag(tagInput: HTMLInputElement): void {
        const firstResult = $('.pac-container .pac-item:first .pac-item-query').text();
        // console.log(' Add tag, in prod mode, firstResult = ' + firstResult);
        const place = firstResult;

        if (tagInput.value.trim() !== '') {
            const tag = {
                [this.displayField]: place
            };
            this.addPredefinedTag(tag);
        }
        tagInput.value = '';

    }

    private addPredefinedTag(tag: Object): void {
        if (this.tags === undefined) {
            this.tags = [];
        }

        if (!this.maximumOfTagsReached()) {
            this.tags.push(tag);
            this.tagsChanged('add', tag);
        }
    }

    private removeTag(tagToRemove: any): void {
        if (!this.isDeleteable(tagToRemove)) {
            return;
        }
        this.tags = this.tags.filter(tag => tagToRemove !== tag);
        this.tagsChanged('remove', tagToRemove);
    }

    private maximumOfTagsReached(): boolean {
        return typeof this.maxTags !== 'undefined' && this.tags.length >= this.maxTags;
    }

    private isDeleteable(tag: any) {
        if (typeof tag.deleteable !== 'undefined' && !tag.deleteable) {
            return false;
        }
        return this.canDeleteTags;
    }

    private typeaheadOnNoMatch(e: any): void {
        if (typeof this.emitNoOptionFound !== 'undefined') {
            this.emitNoOptionFound.emit(e);
        }
    }

    private typeaheadOnSelect(e: TypeaheadMatch): void {
        if (typeof e.item === 'string') {
            this.addPredefinedTag({
                [this.displayField]: e.value
            });
        } else {
            this.addPredefinedTag(e.item);
        }
        this.selected = '';
    }
}
