import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomTagInputComponent } from './customtaginput.component';
import { TypeaheadModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TypeaheadModule.forRoot()
  ],
  declarations: [
    CustomTagInputComponent
  ],
  exports: [
    CustomTagInputComponent
  ]
})
export class CustomTagInputModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CustomTagInputModule,
      providers: []
    };
  }
}
