export const SharedValueList = Object.freeze({
    initialLatitude: 51.673858,
    initialLongitude: 7.815982,
    initialZoom: 6,
    onePolyZoom: 10,
    twoPolyZoom: 7
});
