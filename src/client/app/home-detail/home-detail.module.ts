import { NgModule } from '@angular/core';
import { HomeDetailComponent } from './home-detail.component';
import { HomeDetailRoutingModule } from './home-detail-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [HomeDetailRoutingModule, SharedModule],
    declarations: [HomeDetailComponent],
    exports: [HomeDetailComponent]
})
export class HomeDetailModule { }
