import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeDetailComponent } from './home-detail.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'home-detail', component: HomeDetailComponent }
        ])
    ],
    exports: [RouterModule]
})
export class HomeDetailRoutingModule { }
