import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { NameListService } from '../shared/name-list/name-list.service';
import { RatedHomes } from '../search/ratedhomes';

/**
 * This class represents the lazy loaded HomeDetailComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'sd-homedetail',
    templateUrl: 'home-detail.component.html',
    styleUrls: ['home-detail.component.css'],
})
export class HomeDetailComponent implements OnInit {

    homeId: String;
    ratedHomes: RatedHomes[] = []; //remove later when API is ready
    homeDetails: RatedHomes = new RatedHomes();

    constructor(private activatedRoute: ActivatedRoute, public nameListService: NameListService) { }

    ngOnInit() {
        this.homeId = this.activatedRoute.snapshot.queryParams['id'];
        this.getRatedHomesById(this.homeId);
    }

    getRatedHomesById(homeId: String) {
        const numericId: number = Number(homeId);
        this.nameListService.gethomes()
            .subscribe(
                homes => {
                    this.ratedHomes = homes;
                    this.ratedHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.id === numericId);//remove later when API is ready
                    this.homeDetails = this.ratedHomes[0];
                },
                err => {
                    //console.log(err)
                }
            );
    }
}
