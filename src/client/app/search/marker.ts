export class Marker {
    constructor(public latitude: number,
        public longitude: number,
        public fillColor: string,
        public fillOpacity: number,
        public radius: number,
        public zIndex: number,
        public strokeColor: string,
        public strokeOpacity: number,
        public strokeWeight: number
    ) { }
}
