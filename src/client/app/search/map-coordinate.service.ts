import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MapCoordinateService {

    /**
    * Creates a new MapCoordinateService with the injected HttpClient.
    * @param {HttpClient} http - The injected HttpClient.
    * @constructor
    */
    constructor(private http: HttpClient) { }

    getLatLongForCity(cityVal: string): Observable<Response> {
        const hostname = 'http://18.196.5.35:8089';
        // const hostname = 'http://localhost:8089';
        const params = new HttpParams().set('city', cityVal);
        return this.http.get(hostname + '/test/latlng', { params: params })
            .catch(this.handleError);

    }

    /**
      * Handle HTTP error
      */
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
