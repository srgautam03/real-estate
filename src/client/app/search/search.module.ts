import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { NameListService } from '../shared/name-list/name-list.service';
import { OrderModule } from 'ngx-order-pipe';
import { NgxInputTagModule } from 'ngx-input-tag';
import { MapCoordinateService } from './map-coordinate.service';
import { CustomTagInputModule } from '../customtaginput/customtaginput.module';



@NgModule({
  imports: [SearchRoutingModule, SharedModule, ReactiveFormsModule, NgxPaginationModule, OrderModule, NgxInputTagModule,
    CustomTagInputModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBhRJ9XEkenaPc_INsVMdm3MnHQobYHL-0',
      libraries: ['places']
    })],
  declarations: [SearchComponent],
  exports: [SearchComponent],
  providers: [NameListService, MapCoordinateService]
})
export class SearchModule { }
