import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { NameListService } from '../shared/name-list/name-list.service';
import { RatedHomes } from './ratedhomes';
import { Marker } from './marker';
import { MapTooltip } from './map-tooltip';
import { MapCoordinateService } from './map-coordinate.service';
import { PolyCoordinate } from './polycoordinate';
import { PathDescription } from './pathdescription';
import { SharedValueList } from '../shared/sharedValue/sharedvalue';


/**
 * This class represents the lazy loaded SearchComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.css'],
})
export class SearchComponent implements OnInit {

  public latitude: number;
  public longitude: number;
  public zoom: number;
  cityName: string;


  infoText: String = 'More info';
  infoTextFixedPrice: String = 'More info';
  infoTextClicked: boolean;
  infoTextFixedPriceClicked: boolean;
  showFilterDiv: boolean;
  isShowFilterDivSm: boolean;

  markerInfoVisible: boolean;
  searchForm: FormGroup;

  // for pagination
  p = 1;
  f = 1;

  isSalesTabSelected = true;
  isFixedPriceTabSelected: boolean;

  public ratedHomes: RatedHomes[] = [];
  public saleHomes: RatedHomes[] = [];
  public fixedPriceHomes: RatedHomes[] = [];
  public marker: Marker[] = [];
  public mapTooltip = new MapTooltip();

  public currentHover = -1;
  public currentClick = -1;
  public currentZindex = 1;


  errorMessage: string;
  orderVariable: String = 'publishedDate';

  reverseVariable = true;

  public paths: PolyCoordinate[] = [];
  public pathsList: PathDescription[] = [];

  polygonMaskingList: any[] = [];
  geoJsonObject = {
    'type': 'FeatureCollection',
    'features': [{
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [0, 90],
            [180, 90],
            [180, -90],
            [0, -90],
            [-180, -90],
            [-180, 0],
            [-180, 90],
            [0, 90]
          ]
        ]
      },
      'properties': {}
    }]
  };
  doMasking = false;

  @ViewChild('sortInput') sortInput: ElementRef;

  @ViewChild('search')
  public searchElementRef: ElementRef;

  searchText: String;

  constructor(private _fb: FormBuilder, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, public nameListService: NameListService, public coordinateService: MapCoordinateService) {
    this.searchForm = this._fb.group({
      searchText: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.latitude = SharedValueList.initialLatitude;
    this.longitude = SharedValueList.initialLongitude;
    this.zoom = SharedValueList.initialZoom;
    this.getHomes();
  }


  styleFunc() {
    return {
      fillColor: 'black',
      strokeColor: '#636161',
      strokeWeight: 1.5,
      visible: true
    };
  }

  getLatLongForCity(cityVal: string) {
    this.coordinateService.getLatLongForCity(cityVal).subscribe(
      latLongList => {
        let tempStr = JSON.stringify(latLongList);
        tempStr = tempStr.replace(/"/g, '');
        tempStr = tempStr.replace(/'/g, '"');
        this.paths = JSON.parse(tempStr);
        // console.log(this.paths);
        let cityLatLngList: number[][];
        let i = 0;
        cityLatLngList = [];
        this.doMasking = true;
        for (let p of this.paths) {
          cityLatLngList[i] = [];
          cityLatLngList[i].push(p.lng);
          cityLatLngList[i].push(p.lat);
          i++;
        }
        //***start and end coordinate must be same
        cityLatLngList[i] = [];
        cityLatLngList[i].push(cityLatLngList[0][0]);
        cityLatLngList[i].push(cityLatLngList[0][1]);
        this.polygonMaskingList.push(cityLatLngList);
        this.geoJsonObject = null;
        this.geoJsonObject = {
          'type': 'FeatureCollection',
          'features': [{
            'type': 'Feature',
            'geometry': {
              'type': 'Polygon',
              'coordinates': [
                [
                  [0, 90],
                  [180, 90],
                  [180, -90],
                  [0, -90],
                  [-180, -90],
                  [-180, 0],
                  [-180, 90],
                  [0, 90]
                ]
              ]
            },
            'properties': {}
          }]
        };
        //this.geoJsonObject.features[0].geometry.coordinates.pop();
        for (let _i = 0; _i < this.polygonMaskingList.length; _i++) {
          // console.log("****** ******    ******* i at this.polygonMaskingList:" + this.polygonMaskingList[_i]);
          this.geoJsonObject.features[0].geometry.coordinates.push(this.polygonMaskingList[_i]);
        }
        //this.geoJsonObject.features[0].geometry.coordinates.push(this.testdb);
        this.zoom = SharedValueList.twoPolyZoom;

        const tempPathDesc: PathDescription = new PathDescription();
        tempPathDesc.city = cityVal;
        tempPathDesc.paths = this.paths;
        // console.log(this.paths);
        this.pathsList.push(tempPathDesc);
        this.getSearchedHomes();
        if (this.pathsList.length === 1) {
          const midlength = Math.ceil(this.paths.length / 2);
          this.latitude = this.paths[midlength].lat;
          this.longitude = this.paths[midlength].lng;
          this.zoom = SharedValueList.onePolyZoom;
        } else if (this.pathsList.length === 2) {
          this.zoom = SharedValueList.twoPolyZoom;
        } else {
          this.zoom = SharedValueList.initialZoom;
          this.latitude = SharedValueList.initialLatitude;
          this.longitude = SharedValueList.initialLatitude;
        }
      },
      err => {
        //console.log(err)
      }
    );
  }

  sortBy(sortField: String, reverseOrder: boolean) {
    this.orderVariable = sortField;
    this.reverseVariable = reverseOrder;
    this.sortInput.nativeElement.click();
  }

  customDateComparator(d1: string, d2: string) {
    const date1 = new Date(d1);
    const date2 = new Date(d2);
    return date1 < date2 ? 1 : -1;
  }

  setSalesTabTrue() {
    this.isSalesTabSelected = true;
    this.isFixedPriceTabSelected = false;
  }

  setFixedPriceTabTrue() {
    this.isSalesTabSelected = false;
    this.isFixedPriceTabSelected = true;
  }

  getHomes() {
    this.nameListService.gethomes()
      .subscribe(
        homes => {
          this.ratedHomes = homes;
          this.fixedPriceHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.costType === 'fixed');
          this.saleHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.costType === 'sale');
        },
        err => {
          //console.log(err)
        }

      );
  }

  getSearchedHomes() {
    if (this.pathsList.length === 0) {
      this.fixedPriceHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.costType === 'fixed');
      this.saleHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.costType === 'sale');
      this.latitude = SharedValueList.initialLatitude;
      this.longitude = SharedValueList.initialLongitude;
      this.zoom = SharedValueList.initialZoom;
      return;
    }
    for (const p of this.pathsList) {
      // this.fixedPriceHomes.concat(this.ratedHomes.filter(ratedHomes => ratedHomes.address.includes(p.city)));
      // this.saleHomes.concat(this.ratedHomes.filter(ratedHomes => ratedHomes.address.includes(p.city)));
      this.fixedPriceHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.address.includes(p.city));
      this.saleHomes = this.ratedHomes.filter(ratedHomes => ratedHomes.address.includes(p.city));
    }
  }

  showMarkerInfo(ratedHomes: RatedHomes) {
    this.markerInfoVisible = true;
    this.currentClick = ratedHomes.id;
    this.mapTooltip.heading = ratedHomes.address;
    this.mapTooltip.imageURL = ratedHomes.imageUrl;

  }

  hideMapTooltip() {
    this.markerInfoVisible = false;
    this.currentClick = -1;
  }

  isInfoTextClicked() {
    if (this.infoTextClicked === true) {
      this.infoTextClicked = false;
    } else {
      this.infoTextClicked = true;
    }
    if (this.infoTextClicked) {
      this.infoText = 'Less info';
    } else {
      this.infoText = 'More info';
    }
  }

  isInforDivForFixedPriceClicked() {
    if (this.infoTextFixedPriceClicked === true) {
      this.infoTextFixedPriceClicked = false;
    } else {
      this.infoTextFixedPriceClicked = true;
    }

    if (this.infoTextFixedPriceClicked) {
      this.infoTextFixedPrice = 'Less info';
    } else {
      this.infoTextFixedPrice = 'More info';
    }
  }

  clickFilterDiv() {
    this.showFilterDiv = true;
  }

  showFilterDivSm() {
    this.isShowFilterDivSm = true;
  }

  closeFilterDiv() {
    this.showFilterDiv = false;
    this.isShowFilterDivSm = false;
  }

  hoverMarker(m: RatedHomes) {
    this.currentHover = m.id;
    this.currentClick = -1;
    this.markerInfoVisible = false;
  }

  removeHoverMarker(m: RatedHomes) {
    this.currentHover = -1;
  }

  findIconUrl(m: RatedHomes): String {
    if (m.id === this.currentHover || m.id === this.currentClick)
      return 'assets/img/marker/hoverfin.png';
    else if (m.costType === 'fixed')
      return 'assets/img/marker/fixed.png';
    else if (m.costType === 'sale')
      return 'assets/img/marker/sale.png';
    else
      return 'assets/img/marker/sale.png';
  }

  findZindex(m: RatedHomes): number {
    if (m.id === this.currentHover || m.id === this.currentClick) {
      return 10000;
    } else {
      return 1;
    }
  }

  isSelected(selectedKey: String, isReverse: boolean): String {
    if (this.orderVariable === selectedKey && this.reverseVariable === isReverse) {
      return 'li-checked';
    } else {
      return '';
    }
  }

  onTagsChanged(searchFieldTag: any) {
    this.cityName = searchFieldTag.tag.displayValue;
    if (searchFieldTag.change === 'add') {
      this.getLatLongForCity(this.cityName);
    }
    if (searchFieldTag.change === 'remove') {
      let i = 0;
      for (let p of this.pathsList) {
        if (p.city === this.cityName) {
          this.pathsList.splice(i, 1);
          this.removePolygonMask(i);
        }
        i++;
      }

      this.getSearchedHomes();
    }

  }

  removePolygonMask(position: number) {
    this.geoJsonObject = null;
    this.geoJsonObject = {
      'type': 'FeatureCollection',
      'features': [{
        'type': 'Feature',
        'geometry': {
          'type': 'Polygon',
          'coordinates': [
            [
              [0, 90],
              [180, 90],
              [180, -90],
              [0, -90],
              [-180, -90],
              [-180, 0],
              [-180, 90],
              [0, 90]
            ]
          ]
        },
        'properties': {}
      }]
    };
    this.polygonMaskingList.splice(position, 1);
    if (this.polygonMaskingList.length === 0) {
      this.doMasking = false;
      this.zoom = SharedValueList.initialZoom;
      this.latitude = SharedValueList.initialLatitude;
      this.longitude = SharedValueList.initialLatitude;
    }
    for (let _i = 0; _i < this.polygonMaskingList.length; _i++) {
      // console.log("****** ******    ******* i at this.polygonMaskingList:" + this.polygonMaskingList[_i]);
      this.geoJsonObject.features[0].geometry.coordinates.push(this.polygonMaskingList[_i]);
    }
    if (this.polygonMaskingList.length === 1) {
      this.zoom = SharedValueList.onePolyZoom;
      this.latitude = this.polygonMaskingList[0][0][1];
      this.longitude = this.polygonMaskingList[0][0][0];
    }
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = SharedValueList.initialZoom;
      });
    }
  }
}
