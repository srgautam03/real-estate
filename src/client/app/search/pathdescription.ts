import { PolyCoordinate } from './polycoordinate';
export class PathDescription {
    public city: string;
    public paths: PolyCoordinate[] = [];
}
