export class MapTooltip {
    heading: String;
    info: String;
    logo: String;
    imageURL: String;
    condition: String;
}
